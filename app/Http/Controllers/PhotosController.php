<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhotosController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $photos = Photo::latest()->paginate(6);
        return view('photos.index', compact('photos'));
    }


    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('photos.create');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => ['bail', 'required', 'min:2'],
            'picture' => ['bail', 'required', 'mimes:jpg,png'],
        ]);
        $photo = new Photo();
        $photo->title = $request->input('title');
        if ($request->hasFile('picture')){
            $photo->picture = $request->file('picture')->store('pictures', 'public');
        }
        $photo->user_id = Auth::user()->getAuthIdentifier();
        $photo->save();

        return redirect()->route('photos.index')->with('success', 'photo successfully created');
    }


    /**
     * @param Photo $photo
     * @return Application|Factory|View
     */
    public function show(Photo $photo)
    {
        $total = 0;
        $count = 0;
        $score = 0;
        foreach ($photo->comments as $comment){
            $count++;
            $total += $comment->grade;
        }
        if ($total != 0 and $count != 0){
            $score = $total/$count;
        }

        return view('photos.show', compact('photo', 'score'));
    }


    /**
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function destroy(Photo $photo): RedirectResponse
    {
        $this->authorize('delete', $photo);
        $photo->delete();
        return back()->with('success', 'photo successfully deleted');
    }
}
