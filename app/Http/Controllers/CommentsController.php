<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Photo;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $this->authorize('create', Comment::class);
        $request->validate([
            'grade' => ['bail', 'required', 'numeric', 'regex:/[1-5]/'],
            'description' => ['bail', 'required', 'min:5'],
        ]);
        $comment = new Comment();
        $comment->grade = $request->get('grade');
        $comment->description = $request->input('description');
        $comment->user_id = Auth::user()->getAuthIdentifier();
        $comment->photo_id = $request->input('photo_id');
        $comment->save();
        return back()->with('success', 'comment successfully created');
    }
}
