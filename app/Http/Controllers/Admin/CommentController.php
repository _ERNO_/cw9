<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $comments = Comment::all();
        return view('admin.comments.index', compact('comments'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $users = User::all();
        $photos = Photo::all();
        return view('admin.comments.create', compact('users', 'photos'));
    }

    /**
     * @param CommentRequest $request
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function store(CommentRequest $request): RedirectResponse
    {
        $comment = new Comment($request->all());
        $comment->save();
        return redirect()->route('admin.comments.index')->with('success', 'comment successfully created');
    }


    /**
     * @param Comment $comment
     * @return Application|Factory|View
     */
    public function edit(Comment $comment)
    {
        $users = User::all();
        $photos = Photo::all();
        return view('admin.comments.edit', compact('comment', 'users', 'photos'));
    }


    /**
     * @param CommentRequest $request
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function update(CommentRequest $request, Comment $comment): RedirectResponse
    {
        $comment->update($request->all());
        return redirect()->route('admin.comments.index')->with('success', 'comment successfully updated');
    }


    /**
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function destroy(Comment $comment): RedirectResponse
    {
        $comment->delete();
        return redirect()->route('admin.comments.index')->with('success', 'comment successfully deleted');
    }
}
