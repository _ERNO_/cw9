<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PhotoRequest;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhotoController extends Controller
{

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $photos = Photo::all();
        return view('admin.photos.index', compact('photos'));
    }


    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $users = User::all();
        return view('admin.photos.create', compact('users'));
    }


    /**
     * @param PhotoRequest $request
     * @return RedirectResponse
     */
    public function store(PhotoRequest $request): RedirectResponse
    {
        $photo = new Photo($request->all());
        if ($request->hasFile('picture')){
            $photo->picture = $request->file('picture')->store('pictures', 'public');
        }
        $photo->save();
        return redirect()->route('admin.photos.index')->with('success', "photo success created");
    }


    /**
     * @param Photo $photo
     * @return Application|Factory|View
     */
    public function show(Photo $photo)
    {

        return view('admin.photos.show', compact('photo'));
    }



    /**
     * @param Photo $photo
     * @return Application|Factory|View
     */
    public function edit(Photo $photo)
    {
        $users = User::all();
        return view('admin.photos.edit', compact('photo', 'users'));
    }


    /**
     * @param PhotoRequest $request
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function update(PhotoRequest $request, Photo $photo): RedirectResponse
    {
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $photo->update($data);
        return redirect()->route('admin.photos.index')->with('success', "photo success updated");
    }


    /**
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function destroy(Photo $photo): RedirectResponse
    {
        $photo->delete();
        return redirect()->route('admin.photos.index')->with('success', "photo success deleted");
    }
}
