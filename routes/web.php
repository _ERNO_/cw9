<?php

use App\Http\Controllers\Admin\CommentController;
use App\Http\Controllers\Admin\PhotoController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\LanguageSwitcherController;
use App\Http\Controllers\PhotosController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware('language')->group(function (){


    Route::prefix('admin')->middleware('auth')->name('admin.')->group(function () {
        Route::resources([
            'users' => UserController::class,
            'photos' => PhotoController::class,
            'comments' => CommentController::class,
        ]);
    });

    Route::get('/', [PhotosController::class, 'index'])->name('main');
    Auth::routes();
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('photos', PhotosController::class)->except(['edit', 'update']);
    Route::resource('users', UsersController::class)->except(['create', 'store', 'destroy']);
    Route::resource('comments', CommentsController::class)->only('store');
});


Route::get('language/{locale}', [LanguageSwitcherController::class, 'switcher'])->name('language.switcher')->where('locale', 'en|ru');
