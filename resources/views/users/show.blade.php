@extends('layouts.app')

@section('content')
    <h2>{{$user->name}} @lang('messages.profile')
        @can('update', $user)
        <a class="btn btn-sm btn-outline-primary" href="{{route('users.edit', compact('user'))}}">@lang('messages.edit')</a>
        @endcan
    </h2>
    @can('update', $user)
    <h5>@lang('messages.myphoto'): <a class="btn btn-sm btn-outline-success" href="{{route('photos.create')}}">@lang('messages.create')</a></h5>
    @endcan
    <br>
    <table class="table">
        <tbody>
        @foreach($user->photos as $photo)
        <tr>
            <td>
                <img width="130" height="130" src="{{asset('/storage/' . $photo->picture)}}" alt="{{$photo->picture}}">
            </td>
            <td>
                <a href="{{route('photos.show', compact('photo'))}}">
                    {{$photo->title}}
                </a>
            </td>
            <td>
                @can('delete', $photo)
                <form action="{{route('photos.destroy', compact('photo'))}}" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-sm btn-outline-danger">
                        @lang('messages.delete')
                    </button>
                </form>
                @endcan
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection
