<div class="m-3">
    <a class="btn btn-outline-primary" href="{{route('admin.users.index')}}">@lang('messages.users')</a>
    <a class="btn btn-outline-primary" href="{{route('admin.photos.index')}}">@lang('messages.Laravel')</a>
    <a class="btn btn-outline-primary" href="{{route('admin.comments.index')}}">@lang('messages.comment')</a>
</div>

