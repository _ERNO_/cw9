@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h2>{{$photo->title}}</h2>
            <img width="900" height="500" src="{{asset('/storage/' . $photo->picture)}}" alt="{{$photo->picture}}">
        </div>

        <div class="row" style="margin-top: 20px">
            <div class="col">
               <h5>@lang('messages.everage'): {{$score}}</h5>
            </div>
        </div>
        @if($photo->user_id != \Illuminate\Support\Facades\Auth::user()->getAuthIdentifier())
        @can('create', \App\Models\Comment::class)
                <h3 style="margin-top: 20px">@lang('messages.addComment')</h3>

            <form action="{{action([\App\Http\Controllers\CommentsController::class, 'store'])}}" method="post">
                @csrf

                    <label for="description"
                           class="col-md-4 col-form-label text-md-right">@lang('messages.description')</label>

                    <div class="col-md-6">
                                    <textarea class="form-control @error('description') is-invalid @enderror" id="description"
                                              rows="3" name="description">{{ old('description') }}</textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>

                <input type="hidden" name="photo_id" value="{{$photo->id}}">

                    <label for="institution_id" class="col-md-4 col-form-label text-md-right">@lang('messages.grade')</label>
                    <div class="col-md-6">
                        <select class="custom-select" name="grade">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                <br>
                        <button type="submit" class="btn btn-primary">
                            @lang('messages.create')
                        </button>

            </form>
        @endcan
        @endif
        @foreach($photo->comments as $comment)
            <h5>@lang('messages.by'): {{$comment->user->name}}, {{$comment->grade}}</h5>
            <p>{{$comment->description}}</p>
        @endforeach
    </div>
@endsection
