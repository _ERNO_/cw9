@extends('layouts.app')

@section('content')
    <br>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        @foreach($photos as $photo)
        <div class="col">
            <div class="card">
                <img height="280" width="280" src="{{asset('/storage/' . $photo->picture)}}" class="card-img-top" alt="{{$photo->picture}}">
                <div class="card-body">
                    <h5 class="card-title">{{$photo->title}}</h5>
                    <p class="card-text">@lang('messages.by'): <a href="{{route('users.show', ['user' => $photo->user->id])}}">
                            {{$photo->user->name}}
                        </a></p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-auto">
            {{$photos->links('pagination::bootstrap-4')}}
        </div>
    </div>
@endsection
