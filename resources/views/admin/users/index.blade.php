@extends('layouts.app')

@section('content')
    @include('navbar.navbar')
    <div class="container">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">№</th>
                <th scope="col">@lang('messages.name')</th>
                <th scope="col">@lang('messages.email')</th>
                <th scope="col">@lang('messages.actions')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td class="d-flex">
                        <a class="btn btn-sm btn-outline-success mr-2" href="{{route('admin.users.edit', ['user' => $user])}}">@lang('messages.edit')</a>
                        <form
                            action="{{route('admin.users.destroy' , ['user' => $user])}}"
                            method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-sm btn-outline-danger">
                                @lang('messages.delete')
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
