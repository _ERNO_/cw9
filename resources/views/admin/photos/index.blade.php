@extends('layouts.app')

@section('content')
    @include('navbar.navbar')
    <a class="btn btn-outline-success" href="{{route('admin.photos.create')}}">@lang('messages.create')</a>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">№</th>
                <th scope="col">@lang('messages.photo')</th>
                <th scope="col">@lang('messages.title')</th>
                <th scope="col">@lang('messages.author')</th>
                <th scope="col">@lang('messages.actions')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($photos as $photo)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>
                        <img height="150" width="150" src="{{asset('/storage/' . $photo->picture)}}" alt="{{$photo->picture}}">
                    </td>
                    <td><a href="{{route('admin.photos.show', compact('photo'))}}">
                            {{$photo->title}}
                        </a></td>
                    <td>{{$photo->user->name}}</td>
                    <td class="d-flex">
                        <a class="btn btn-sm btn-outline-success mr-2" href="{{route('admin.photos.edit', ['photo' => $photo])}}">@lang('messages.edit')</a>
                        <form
                            action="{{route('admin.photos.destroy' , ['photo' => $photo])}}"
                            method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-sm btn-outline-danger">
                                @lang('messages.delete')
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
@endsection

