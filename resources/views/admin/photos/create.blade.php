@extends('layouts.app')

@section('content')
    @error('picture')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">@lang('messages.create')</div>
                    <div class="card-body">
                        <form action="{{route('admin.photos.store')}}" method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="title" class="col-md-4 col-form-label text-md-right">@lang('messages.title')</label>
                                <div class="col-md-6">
                                    <input class="form-control @error('title') is-invalid @enderror" type="text"
                                           id="title" name="title"
                                           value="{{ old('title') }}"/>
                                    @error('title')
                                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <br>

                            <div class="form-group row">
                                <label for="picture"
                                       class="col-md-4 col-form-label text-md-right">@lang('messages.photo')</label>

                                <div class="col-md-6">
                                    <input name="picture" type="file" class="custom-file-input @error('picture') is-invalid @enderror" id="picture" aria-describedby="picture">
                                    @error('picture')
                                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <br>

                            <div class="form-group row">
                                <label for="user_id" class="col-md-4 col-form-label text-md-right">@lang('messages.users')</label>
                                <div class="col-md-6">
                                    <select class="custom-select" name="user_id">
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <br>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-outline-success">@lang('messages.create')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

@endsection
