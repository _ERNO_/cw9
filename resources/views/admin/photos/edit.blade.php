@extends('layouts.app')

@section('content')
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">@lang('messages.edit')</div>
                    <div class="card-body">
                        <form action="{{route('admin.photos.update', ['photo' => $photo])}}" method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <label for="title" class="col-md-4 col-form-label text-md-right">@lang('messages.title')</label>

                                <div class="col-md-6">
                                    <input class="form-control @error('title') is-invalid @enderror" type="text"
                                           id="title" name="title"
                                           value="{{ $photo->title }}"/>
                                    @error('title')
                                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <br>

                            <div class="form-group row">
                                <label for="picture"
                                       class="col-md-4 col-form-label text-md-right">@lang('messages.photo')</label>

                                <div class="col-md-6">
                                    <input name="picture" type="file" class="custom-file-input @error('picture') is-invalid @enderror" id="picture" aria-describedby="picture">
                                    @error('picture')
                                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user_id" class="col-md-4 col-form-label text-md-right">@lang('messages.users')</label>
                                <div class="col-md-6">
                                    <select class="custom-select" name="user_id">
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-outline-success">Update</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
