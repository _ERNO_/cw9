@extends('layouts.app')

@section('content')
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <form action="{{route('admin.comments.store')}}" method="post">
                        @csrf

                        <div class="form-group row">
                            <label for="institution_id" class="col-md-4 col-form-label text-md-right">@lang('messages.grade')</label>
                            <div class="col-md-6">
                                <select class="custom-select" name="grade">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="user_id" class="col-md-4 col-form-label text-md-right">@lang('messages.users')</label>
                            <div class="col-md-6">
                                <select class="custom-select" name="user_id">
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="photo_id" class="col-md-4 col-form-label text-md-right">@lang('messages.photo')</label>
                            <div class="col-md-6">
                                <select class="custom-select" name="photo_id">
                                    @foreach($photos as $photo)
                                        <option value="{{$photo->id}}">
                                            {{$photo->title}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description"
                                   class="col-md-4 col-form-label text-md-right">@lang('messages.description')</label>

                            <div class="col-md-6">
                                    <textarea class="form-control @error('description') is-invalid @enderror" id="description"
                                              rows="3" name="description">{{ old('description') }}</textarea>
                                @error('description')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-6">
                                <button type="submit" class="btn btn-primary">
                                    @lang('messages.create')
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
