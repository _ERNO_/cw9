@extends('layouts.app')

@section('content')
    @include('navbar.navbar')
    <a class="btn btn-outline-success" href="{{route('admin.comments.create')}}">@lang('messages.create')</a>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">@lang('messages.description')</th>
            <th scope="col">@lang('messages.grade')</th>
            <th scope="col">@lang('messages.author')</th>
            <th scope="col">@lang('messages.photo')</th>
            <th scope="col">@lang('messages.actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($comments as $comment)
            <tr>
                <td>
                    {{$loop->iteration}}
                </td>
                <td>{{$comment->description}}</td>
                <td>{{$comment->grade}}</td>
                <td>{{$comment->user->name}}</td>
                <td>
                    <img width="100" height="100" src="{{asset('/storage/' . $comment->photo->picture)}}" alt="{{$comment->photo->picture}}">
                </td>
                <td class="d-flex">
                    <a class="btn btn-sm btn-outline-success mr-2" href="{{action([\App\Http\Controllers\Admin\CommentController::class, 'edit'], ['comment' => $comment])}}">@lang('messages.edit')</a>
                    <form
                        action="{{action([\App\Http\Controllers\Admin\CommentController::class, 'destroy'] , ['comment' => $comment])}}"
                        method="post">
                        @csrf
                        @method('delete')
                        <button class="btn btn-sm btn-outline-danger">
                            @lang('messages.delete')
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
