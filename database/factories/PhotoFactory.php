<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PhotoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->word(),
            'picture' => $this->getImage(rand(1, 17)),
        ];
    }


    /**
     * @param int $image_number
     * @return string
     */
    private function getImage($image_number = 1): string
    {
        $path = storage_path() . "/photos/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/'.$image_name, $resize->__toString());
        return 'pictures/'.$image_name;
    }
}
