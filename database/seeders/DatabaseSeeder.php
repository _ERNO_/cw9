<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = User::factory()->count(3)->create();
        foreach ($users as $user){
            $photos = Photo::factory()->count(10)->for($user)->create();
            foreach ($photos as $photo){
                Comment::factory()->count(3)->for($user)->for($photo)->create();
            }
        }
    }
}
